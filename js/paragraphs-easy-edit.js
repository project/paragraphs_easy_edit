(function (Drupal, $, once) {
  Drupal.behaviors.paragraphsEasyEditAjaxForm = {
    attach: function (context, settings) {
      $(once('easy-edit-click', '.easy-edit-toggle')).click(function (event) {

        var entity_type = 'paragraph';
        var toggle = $(event.target);
        var entity_id = toggle.data('id');
        var paragraph = $('.paragraph[data-id="' + entity_id + '"]');
        var form_container = $('.easy-edit[data-id="' + entity_id + '"]');

        if (paragraph.hasClass('easy-edit-active')){
          // close paragraph form
          form_container.html('');
          paragraph.toggleClass('easy-edit-active');
          toggle.text('edit');
        } else {
          // fetch paragraph form
          $.ajax({
            url: Drupal.url('paragraphs-easy-edit/ajax-form/' + entity_type + '/' + entity_id),
            type: 'GET',
            dataType: 'json',
            success: function (response) {
              // Update the target element with the form.
              form_container.html(response.form);
              form = $(form_container.find('form'));
              form.attr('data-id', entity_id);
              paragraph.toggleClass('easy-edit-active');
              toggle.text('close');
            }
          });
        };
      });

      // Add form submission handling to the existing behavior
      $('.easy-edit').on('submit', 'form', function(event) {
        event.preventDefault(); // Prevent default form submission behavior

        // Retrieve the paragraph ID from the form
        var entity_id = $(this).data('id');

        // Serialize form data
        var formData = $(this).serialize();

        // Submit form via AJAX
        $.ajax({
          url: $(this).attr('action'),
          type: $(this).attr('method'),
          data: formData + '&paragraph_id=' + entity_id, // Include paragraph ID in the data
          dataType: 'json',
          success: function(response) {
            // Handle success response
            location.reload();
          },
          error: function(xhr, status, error) {
            // Handle error response
          }
        });
      });
    }
  };
})(Drupal, jQuery, once);
