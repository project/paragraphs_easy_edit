<?php

namespace Drupal\paragraphs_easy_edit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class EasyEditController extends ControllerBase {

  protected $entityTypeManager;
  protected $renderer;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  public function ajaxForm($entity_type, $entity_id) {
    // Load the entity.
    $entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($entity_id);

    // Build the form.
    $form = $this->entityFormBuilder()->getForm($entity);

    // Return form as JSON.
    return new JsonResponse([
      'form' => $this->renderer->renderRoot($form)
    ]);
  }

}
